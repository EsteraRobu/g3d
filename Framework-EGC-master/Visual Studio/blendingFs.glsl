#version 330
  in vec2 TexCoord;
  uniform float mixValue;
  uniform sampler2D texture1;
   uniform sampler2D texture2;
  void main()
   {
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), mixValue)* vec4(ourColor,1.0);
   }