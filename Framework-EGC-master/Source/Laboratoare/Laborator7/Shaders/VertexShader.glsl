#version 330

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texture_coord;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

// Uniforms for light properties
uniform vec3 light_position;
uniform vec3 eye_position;
uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

// Output value to fragment shader
out vec3 color;

void main()
{
	// TODO: compute world space vectors
	vec3 world_pos = (Model * vec4(v_position, 1)).xyz;
	vec3 world_normal = normalize(mat3(Model) * v_normal);
	vec3 N = normalize(v_normal);
	vec3 L = normalize(light_position - world_pos);
	vec3 V = normalize(eye_position - world_pos);
	vec3 H = normalize(L + V);
	vec3 R = reflect(L,N);

	// TODO: define ambient light component
	float ambient_light = 0.25;
	float ambientala = material_kd * ambient_light;

	// TODO: compute diffuse light component
	float diffuse_light = 0;
	if(dot(L,N) >= 0)
		diffuse_light = 1;
	float difuza = diffuse_light * max(dot(N,L), 0);

	// TODO: compute specular light component
	float specular_light = 0;
	float pr;
	if(dot(N,L) > 0)
		pr = 1;
	else
		pr = 0;

	float speculara = material_ks * specular_light * pr * pow(max(dot(V,R), 0), material_shininess);
	//float speculara = material_ks * specular_light * pr * pow(max(dot(N,H), 0), material_shininess);

	if (diffuse_light > 0)
	{

	}

	float d = length(world_pos - light_position);
	float c = 1, l = 0.0002, q = 0.0005;
	float atten = 1 / (c + l * d + q * d * d);

	// TODO: compute light

	// TODO: send color light output to fragment shader

	color = object_color * (ambientala + atten * (speculara + difuza));
	 

	gl_Position = Projection * View * Model * vec4(v_position, 1.0);
}
