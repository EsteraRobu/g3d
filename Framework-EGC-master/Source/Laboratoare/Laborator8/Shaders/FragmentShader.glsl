#version 330

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

// Uniforms for light properties
uniform vec3 light_direction;
uniform vec3 light_position;
uniform vec3 eye_position;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;
uniform float cut_off;
uniform int mode;

uniform vec3 object_color;

layout(location = 0) out vec4 out_color;

void main()
{
	vec3 N = world_normal;
	vec3 L = normalize(light_position - world_position);
	vec3 V = normalize(eye_position - world_position);
	vec3 H = normalize(L + V);
	vec3 R = normalize(reflect(L, N));
	// TODO: define ambient light component
	float ambient_light = 0.25;
	vec3 ambientala = vec3(0.2, 0.2, 0.2);

	// TODO: compute diffuse light component
	vec3 diffuse_light = vec3(1,1,1);
	vec3 difuza = diffuse_light * object_color * max(dot(N, L), 0);

	// TODO: compute specular light component
	float specular_light = 0;
	float primesteLumina;
	if (dot(N, L) > 0)
		primesteLumina = 1.0;
	else
		primesteLumina = 0.0;
	float speculara = material_ks * specular_light * primesteLumina * pow(max(dot(V, R), 0), material_shininess);

	float atten;
	float d = distance(light_position, world_position);

	atten = 1 / (d * d);

	// TODO: compute light

	// TODO: write pixel out color

	float spot_light_limit = cos(cut_off);
	float spot_light = dot(-L, light_direction);
	float linear_att = (spot_light - spot_light_limit) / (1 - spot_light_limit);
	float light_att_factor = pow(linear_att, 2);

	if (mode == 0) {
		out_color = vec4(ambientala + atten * (difuza + speculara), 1);
	} else {
		if (spot_light > cos(cut_off)) {
			out_color = vec4(ambientala + light_att_factor * (difuza + speculara), 1);
		}
		else {
			out_color = vec4(ambientala, 1);
		}
	}
	

	
}