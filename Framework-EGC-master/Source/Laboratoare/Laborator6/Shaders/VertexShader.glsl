#version 330

// TODO: get vertex attributes from each location
layout(location = 0) in vec3 v_position;
layout(location = 3) in vec3 normal;
layout(location = 1) in vec3 color;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform float angle;

// TODO: output values to fragment shader
out vec3 frag_color;
out vec3 frag_normal;

void main()
{
	// TODO: send output to fragment shader
	frag_color = color* angle;
	frag_normal = normal ;
	// Eventual se face asta si pt v_position si pt textura (location=2).
	// TODO: compute gl_Position
	gl_Position = Projection * View * Model * vec4(v_position + angle, 1.0);
}
