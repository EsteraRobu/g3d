#pragma once
#include <Component/SimpleScene.h>
#include <Component/Transform/Transform.h>
#include <Core/GPU/Mesh.h>
#include "LabCamera.h"

using namespace std;

class Laborator9 : public SimpleScene
{
	public:
		Laborator9();
		~Laborator9();

		

		Mesh * CreateMesh(const char * name, const std::vector<VertexFormat>& vertices, const std::vector<unsigned short>& indices);

		void Init() override;

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void RenderLightMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, Texture2D* texture1 = NULL, Texture2D* texture2 = NULL);

		void RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 &modelMatrix, Texture2D* texture1 = NULL, Texture2D* texture2 = NULL);
		Texture2D* CreateRandomTexture(unsigned int width, unsigned int height);

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;
		Laborator::Camera *camera;
		glm::mat4 projectionMatrix;
		bool renderCameraTarget;
		float fov;
		float dx1, dx2, dy1, dy2;
		int ok;

		int flag, rotationFlag, num, dino, planetLight, planetMove;
		float heightUnit;
		float angle[20];
		glm::vec3 lightPosition[10], lightDirection[10];
		unsigned int materialShininess;
		glm::vec3 materialKd, materialKs;
		int pause, rotate, skyRotate, steppedLighting, numLevels, shadow;
		glm::vec3 earthPosition;
		float rotationAngle, ka;
		std::unordered_map<std::string, Texture2D*> mapTextures;
		GLuint randomTextureID;
		//int flag;
};
