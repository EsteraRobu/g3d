#version 330
 
uniform sampler2D texture_1;
uniform sampler2D texture_2;
 
in vec2 texcoord;
uniform int flag;
uniform float time;

layout(location = 0) out vec4 out_color;

void main()
{
	// TODO : calculate the out_color using the texture2D() function
	vec4 color = texture2D(texture_1, texcoord + vec2(1,0) * time);

	if(flag != 1 && flag!=2 && flag!= 3  && flag!= 4)
		if (texture2D(texture_2, texcoord).x > 0.5) {
			discard;
		}
		
		if(flag==2 && flag!= 4){
		if (texture2D(texture_2, texcoord).x < 0.5) {
			discard;
		}}
	//		if(flag ==2)
	//	{
	//		if (color ==vec4(0,0,0,0)) 
	//		{
		//		discard;
		//}
		//}
	out_color =color;// mix(color, vec4(0.5f,0.9f,0.7f,1), 0.5f);
}