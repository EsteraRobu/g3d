#version 330
 
in vec3 world_position;
in vec3 world_normal;

uniform sampler2D texture_1;
uniform sampler2D texture_2;
 
in vec2 texcoord;
uniform int rotationFlag;
uniform float time;
uniform int num;
uniform int steppedLighting;
uniform int numLevels;
uniform int flag;
uniform vec3 material_kd;
uniform vec3 material_ks;
uniform int material_shininess;

//uniform vec3 light_direction[10];
uniform vec3 light_pos1;
uniform vec3 light_pos2;
uniform vec3 light_pos3;
uniform vec3 light_pos4;
uniform vec3 light_pos5;
uniform vec3 light_pos6;
uniform vec3 light_pos7;
uniform vec3 eye_position;

layout(location = 0) out vec4 out_color;

void main()
{
	vec4 color, color2;
	int i;

	vec3 kd = material_kd;
	vec3 ks = material_ks;

	vec3 N = world_normal;
	vec3 L, H, R;
	vec3 V = normalize(eye_position - world_position);

	//Culorile luminilor
	vec3 lightColor[10];
	lightColor[1] = vec3(0.1f, 0.8f, 0.3f);
	lightColor[2] = vec3(0.7f, 0.7f, 0.3f);
	lightColor[3] = vec3(0.2f, 0.4f, 0.7f);
	lightColor[4] = vec3(0.5f, 0.2f, 0.6f);
	lightColor[5] = vec3(0.7f, 0.7f, 0.0f);
	lightColor[6] = vec3(0.9f, 0.2f, 0.6f);
	lightColor[7] = vec3(1, 1, 1);

	//Pozitiile luminilor
	vec3 light_position[10];
	light_position[1] = light_pos1;
	light_position[2] = light_pos2;
	light_position[3] = light_pos3;
	light_position[4] = light_pos4;
	light_position[5] = light_pos5;
	light_position[6] = light_pos6;
	light_position[7] = light_pos7;

	color = texture2D(texture_1, texcoord);

	if(num == 2 && rotationFlag == 1)
		color = texture2D(texture_1, texcoord + vec2(1,0) * time / 60);

	//Facem o copie la color deoarece vrem ca fundalul sa nu fie iluminat
	color2 = color;

	vec3 objColor = vec3(color);
	vec3 oColor = vec3(0, 0, 0);

	if(num == 20) {
		objColor = vec3(1, 1, 1);
		kd = texture2D(texture_1, texcoord).rgb;
		ks = texture2D(texture_2, texcoord).rgb;
	}

	float recvLight = 0;

	float c, l, q, d, atten;
	//Coeficientii folositi pentru atenuare
	c =0.0001; l = 0.0002; q = 0.05;

	for(i = 1; i <= 7; ++i) {
		L = normalize(light_position[i] - world_position);
		H = normalize(L + V);
		R = normalize(reflect(-L, N));

		recvLight = 0;
		if (dot(N, L) > 0)
			recvLight = 1;

		d = distance(light_position[i], world_position);
		atten = 1 / (c + l * d + q * d * d);

		float i1 = clamp(dot(N, L), 0, 1);
		float i2 = pow(clamp(dot(V, R), 0, 1), material_shininess);

		//Stepped Lighting
		if(steppedLighting == 1) {
			i1 = floor(i1 * numLevels) / numLevels;
			i2 = floor(i2 * numLevels) / numLevels;
		}

		oColor += ((0.2 + i1) * kd + recvLight * i2 * ks) 
				* objColor * lightColor[i] * atten;
	}

	//Iluminarea nu se aplica la background
	if(num == 2)
		oColor = vec3(color2);

	//Pentru ground, dam discard la anumiti pixeli
	if(num == 1)
		if (texture2D(texture_1, texcoord).x < 0.05) {
			discard;
		}
if(flag != 1 && flag!=2 && flag!= 3  && flag!= 4)
		if (texture2D(texture_2, texcoord).x > 0.5) {
			discard;
		}
		
		if(flag==2 && flag!= 4){
		if (texture2D(texture_2, texcoord).x < 0.5) {
			discard;
		}}
	out_color = vec4(oColor, 1);
}