#include "Laborator9.h"

#include <vector>
#include <string>
#include <iostream>
#include <Core/Engine.h>
//
using namespace std;


Laborator9::Laborator9()
{
}

Laborator9::~Laborator9()
{
}
float unghi = 0.03;
int numLines = 70;
int numPoints = 50;
float coneAngle = tan(M_PI * 45.0f / 180.0f);
float limitAngle = 10000 * M_PI;
float coneHeight = 3.5f;
// Rotate matrix relative to the OZ axis
inline glm::mat4 RotateOY(float radians)
{
	// TODO implement rotate matrix
	return glm::transpose(
		glm::mat4(cos(radians), 0, sin(radians), 0,
			0, 1, 0, 0,
			-sin(radians), 0, cos(radians), 0,
			0, 0, 0, 1)
	);
}
// Light attributes
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

vector<VertexFormat> getSphereVertices() {
	vector<VertexFormat> vertices;
	vector<VertexFormat> secondary;
	glm::vec3 color = glm::vec3(1, 1, 1);
	int i, j;
	float sh, h, r, angle, c, s;
	float angleUnit = 2 * M_PI / (float)numPoints;
	float d = 1.0 / (float)numPoints;

	vertices.push_back(VertexFormat(glm::vec3(0, 1, 0), color, glm::vec3(0, 1, 0), glm::vec2(0, 1)));
	for (i = 1; i <= numLines; ++i) {
		h = 1.0 - i * 1.0 / (float)numLines;
		sh = 0.5 + h / 2.0;
		r = sqrt(1 - h * h);

		secondary.push_back(VertexFormat(glm::vec3(r, h, 0), color, glm::vec3(r, h, 0), glm::vec2(1, sh)));

		angle = 0.0;
		for (j = 0; j < numPoints; ++j) {
			c = cos(angle) * r;
			s = sin(angle) * r;
			vertices.push_back(VertexFormat(glm::vec3(c, h, s), color, glm::vec3(c, h, s), glm::vec2((float)j * d, sh)));
			angle += angleUnit;
		}
	}

	for (i = 1; i < numLines; ++i) {
		h = i * 1.0 / (float)numLines;
		sh = 0.5 - h / 2;
		r = sqrt(1 - h * h);

		secondary.push_back(VertexFormat(glm::vec3(r, -h, 0), color, glm::vec3(r, -h, 0), glm::vec2(1, sh)));
		angle = 0.0;
		for (j = 0; j < numPoints; ++j) {
			c = cos(angle) * r;
			s = sin(angle) * r;
			vertices.push_back(VertexFormat(glm::vec3(c, -h, s), color, glm::vec3(c, -h, s), glm::vec2((float)j * d, sh)));
			angle += angleUnit;
		}
	}

	vertices.push_back(VertexFormat(glm::vec3(0, -1, 0), color, glm::vec3(0, -1, 0), glm::vec2(0, 0)));

	int N = secondary.size();
	for (i = 0; i < N; ++i)
		vertices.push_back(secondary[i]);

	return vertices;
}

vector<unsigned short> getSphereIndices() {
	vector<unsigned short> indices;
	int i, j, n, c1, c2, nod;
	n = numLines * 2 - 2;
	nod = (2 * numLines - 1) * numPoints + 2;

	for (i = 1; i < numPoints; ++i) {
		indices.push_back(0);
		indices.push_back(i + 1);
		indices.push_back(i);
	}
	indices.push_back(0);
	indices.push_back(nod);
	indices.push_back(numPoints);

	c1 = 1;
	c2 = numPoints + 1;
	for (i = 0; i < n; ++i) {
		for (j = 1; j < numPoints; ++j) {
			indices.push_back(c1);
			indices.push_back(c1 + 1);
			indices.push_back(c2);
			indices.push_back(c2);
			indices.push_back(c1 + 1);
			indices.push_back(c2 + 1);
			++c1;
			++c2;
		}
		indices.push_back(c1);
		indices.push_back(nod);
		indices.push_back(c2);
		indices.push_back(c2);
		indices.push_back(nod);
		indices.push_back(nod + 1);
		++nod;
		++c1;
		++c2;
	}

	for (i = 1; i < numPoints; ++i) {
		indices.push_back(c1);
		indices.push_back(c1 + 1);
		indices.push_back(c2);
		++c1;
	}
	indices.push_back(c1);
	indices.push_back(nod);
	indices.push_back(c2);

	return indices;
}

Mesh* Laborator9::CreateMesh(const char *name, const std::vector<VertexFormat> &vertices, const std::vector<unsigned short> &indices)
{
	unsigned int VAO = 0;
	// TODO: Create the VAO and bind it
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// TODO: Create the VBO and bind it
	unsigned int VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// TODO: Send vertices data into the VBO buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	// TODO: Crete the IBO and bind it
	unsigned int IBO;
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

	// TODO: Send indices data into the IBO buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	// ========================================================================
	// This section describes how the GPU Shader Vertex Shader program receives data

	// set vertex position attribute
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), 0);

	// set vertex normal attribute
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(glm::vec3)));

	// set texture coordinate attribute
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3)));

	// set vertex color attribute
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3) + sizeof(glm::vec2)));
	// ========================================================================

	// Unbind the VAO
	glBindVertexArray(0);

	// Check for OpenGL errors
	CheckOpenGLError();

	// Mesh information is saved into a Mesh object
	meshes[name] = new Mesh(name);
	meshes[name]->InitFromBuffer(VAO, static_cast<unsigned short>(indices.size()));
	meshes[name]->vertices = vertices;
	meshes[name]->indices = indices;
	return meshes[name];

}

void Laborator9::Init()
{
	//cout << "init";
	renderCameraTarget = false;
	{ //Shader pentru lumini
		Shader *shader2 = new Shader("Shader2");
		shader2->AddShader("Source/Laboratoare/Laborator9/Shaders/VertexShaderLight.glsl", GL_VERTEX_SHADER);
		shader2->AddShader("Source/Laboratoare/Laborator9/Shaders/FragmentShaderLight.glsl", GL_FRAGMENT_SHADER);
		shader2->CreateAndLink();
		shaders[shader2->GetName()] = shader2;
	}
	//Date pentru lumina provenita de la soare
	lightPosition[1] = glm::vec3(14, 1, 15);
	lightPosition[2] = glm::vec3(0, 2, 0);
	lightPosition[3] = glm::vec3(-40, 1, -5);
	lightPosition[4] = glm::vec3(-5, 2, 35);
	lightPosition[5] = glm::vec3(-5,2 , 10);
	lightPosition[6] = glm::vec3(0, 2, 0);
	lightPosition[7] = glm::vec3(-30, 2, 0);
	lightDirection[7] = glm::vec3(1, 1, 1);

	camera = new Laborator::Camera();
	camera->Set(glm::vec3(0, 2, 3.5f), glm::vec3(0, 1, 0), glm::vec3(0, 1, 0));

	fov = window->props.aspectRatio;
	projectionMatrix = glm::perspective(RADIANS(60), window->props.aspectRatio, 0.01f, 200.0f);
	dx1 = -30.0f;
	dx2 = 30.0f;
	dy1 = -30.0f;
	dy2 = 30.0f;
	ok = 0;
	const string textureLoc = "Source/Laboratoare/Laborator9/Textures/";

	// Load textures
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D((textureLoc + "crate.jpg").c_str(), GL_REPEAT);
		mapTextures["crate"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Horse/Horse.jpg", GL_REPEAT);
		mapTextures["Horse"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Fire/fireplace.png", GL_REPEAT);
		mapTextures["Fire"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/wood/WoodTexture.png", GL_REPEAT);
		mapTextures["Wood"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house1/house5.png", GL_REPEAT);
		mapTextures["house1"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house1/house5.png", GL_REPEAT);
		mapTextures["house1"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/Forest_trees.jpg", GL_REPEAT);
		mapTextures["mountain"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/sky1.jpg", GL_REPEAT);
		mapTextures["sky"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/Grass-small.png", GL_REPEAT);
		mapTextures["grass"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/rosetta-stone.jpg", GL_REPEAT);
		mapTextures["stone"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/clouds2.png", GL_REPEAT);
		mapTextures["clouds"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/tree/maple_bark.png", GL_REPEAT);
		mapTextures["treebark"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/tree/maple_leaf.png", GL_REPEAT);
		mapTextures["treeleaf"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/Farmhouse_Texture.png", GL_REPEAT);
		mapTextures["house2"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseB.png", GL_REPEAT);
		mapTextures["houseB"] = texture;
	} {
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseR.png", GL_REPEAT);
		mapTextures["houseR"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/Farmhouse.png", GL_REPEAT);
		mapTextures["house2F"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseB.png", GL_REPEAT);
		mapTextures["houseQ"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/Farmhouse.png", GL_REPEAT);
		mapTextures["houseG"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseR.png", GL_REPEAT);
		mapTextures["houseW"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/Farmhouse_Texture.png", GL_REPEAT);
		mapTextures["houseK"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseB.png", GL_REPEAT);
		mapTextures["houseX"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/Farmhouse_Texture.png", GL_REPEAT);
		mapTextures["houseV"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/house2/houseR.png", GL_REPEAT);
		mapTextures["houseJ"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D((textureLoc + "Resources/Models/banca/wood.png").c_str(), GL_REPEAT);
		mapTextures["wood"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Vegetation/7041417-lava-texture-in-horizontal-composition.jpg", GL_REPEAT);
		mapTextures["sun"] = texture;
	}
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Resources/Models/Props/concrete.png", GL_REPEAT);
		mapTextures["Lamp"] = texture;
	}
	{
		mapTextures["random"] = CreateRandomTexture(25, 25);
		mapTextures["random2"] = CreateRandomTexture(25, 25);
	}
	steppedLighting = 0;
	numLevels = 2;

	/*{
		Mesh* mesh = new Mesh("house1");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "house1", "Gost_House.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}*/
	vector<VertexFormat> sphereVertices = getSphereVertices();
	vector<unsigned short> sphereIndices = getSphereIndices();
	CreateMesh("sfera", sphereVertices, sphereIndices);
	{
		vector<glm::vec3> vertices
		{
			glm::vec3(-0.5f,  0.0f, 0.0f),	// Top Right
			glm::vec3(0.5f, -0.0f, 0.0f),	// Bottom Right
			glm::vec3(0.0f, 0.5f, 0.0f),	// Bottom Left
			glm::vec3(0.0f,  0.0f, 0.5f),	// Top Left
		};

		vector<glm::vec3> normals
		{
			glm::vec3(1.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 1.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f),
			glm::vec3(1.0f, 1.0f, 0.0f)
		};

		// TODO : Complete texture coordinates for the square
		vector<glm::vec2> textureCoords
		{
			glm::vec2(1.0f, 1.0f),
			glm::vec2(1.0f, 0.0f),
			glm::vec2(0.0f, 0.0f),
			glm::vec2(0.0f, 1.0f)
		};

		vector<unsigned short> indices =
		{
			 0, 2, 3,
			 3,2,1,
			 1, 2, 0,
			 1,3,0
		};
		Mesh* mesh = new Mesh("mountain");
		mesh->InitFromData(vertices, normals, textureCoords, indices);
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		vector<glm::vec3> vertices
		{
			glm::vec3(0.5f,   0.5f, 0.0f),	// Top Right
			glm::vec3(0.5f,  -0.5f, 0.0f),	// Bottom Right
			glm::vec3(-0.5f, -0.5f, 0.0f),	// Bottom Left
			glm::vec3(-0.5f,  0.5f, 0.0f),	// Top Left
		};

		vector<glm::vec3> normals
		{
			glm::vec3(0, 1, 1),
			glm::vec3(1, 0, 1),
			glm::vec3(1, 0, 0),
			glm::vec3(0, 1, 0)
		};

		// TODO : Complete texture coordinates for the square
		vector<glm::vec2> textureCoords
		{ glm::vec2(-1.0f,  -1.0f),
			glm::vec2(-1.0f,   1.0f),
			glm::vec2(1.0f,   1.0f),
			glm::vec2(1.0f,   -1.0f)
		};

		vector<unsigned short> indices =
		{
			0, 1, 3,
			1, 2, 3
		};

		Mesh* mesh = new Mesh("square");
		mesh->InitFromData(vertices, normals, textureCoords, indices);
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("Horse");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Horse", "Horse.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("Wood");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "wood", "Wood.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("Fire");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Fire", "fireplace.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("house2");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "house2", "farmhouse_obj.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("banca");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "banca", "bench_w_spine.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("mountainss");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Vegetation", "lowpolymountains.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("grass");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Vegetation", "BasicGrass_02.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("treebark");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "tree", "MapleTreeStem.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("treeleaf");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "tree", "MapleTreeLeaves.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	{
		Mesh* mesh = new Mesh("Lamp");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Lamp", "StreetLamp.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}
	//Constanta folosita pentru "Stepped Lighting"
	steppedLighting = 0;
	numLevels = 2;
	//Constanta folosita pentru contur
	shadow = 0;
	//Constanta folosita pentru luminarea planetelor
	planetLight = 1;
	//{
	//	Mesh* mesh = new Mesh("house2");
	//	mesh->LoadMesh(RESOURCE_PATH::MODELS + "house2", "farmhouse_obj.obj");
	//	meshes[mesh->GetMeshID()] = mesh;
	//}




	// Create a shader program for drawing face polygon with the color of the normal
	{
		Shader *shader = new Shader("ShaderLab9");
		shader->AddShader("Source/Laboratoare/Laborator9/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Laborator9/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}
	//cout << "finish init";

	{ //Date pentru lumina mare care se pliba pe un con
		//float posy = ((float)rand()) / RAND_MAX * coneHeight;
		//float R = (3.0 - posy) * coneAngle;
		//angle[1] = ((float)rand()) / RAND_MAX * 2.0 * M_PI;
		//heightUnit = 0.005f;

		//lightPosition[1] = glm::vec3(R * sin(angle[1]), posy, R * cos(angle[1]));
		//lightDirection[1] = glm::vec3(0, -1, 0);
		materialShininess = 3;
		materialKd = glm::vec3(1, 1, 1);
		materialKs = glm::vec3(0, 0, 0);
	}
}

void Laborator9::FrameStart()
{
	//cout << "farmeStart";
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}
inline glm::mat4 RotateOX(float radians)
{
	return glm::transpose(
		glm::mat4(1, 0, 0, 0,
			0, cos(radians), -sin(radians), 0,
			0, sin(radians), cos(radians), 0,
			0, 0, 0, 1)
	);
}
inline glm::mat4 RotateOZ(float radians)
{
	// TODO implement rotate matrix
	return glm::transpose(
		glm::mat4(cos(radians), -sin(radians), 0, 0,
			sin(radians), cos(radians), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1)
	);
}
void Laborator9::Update(float deltaTimeSeconds)
{
	//std::cout << "start update";
	{ // Background
		glm::mat4 modelMatrix = glm::mat4(1);;
		modelMatrix = glm::scale(modelMatrix, glm::vec3(100.0f));
			modelMatrix = glm::rotate(modelMatrix, RADIANS(180.0f), glm::vec3(1,0, 0));

		RenderSimpleMesh(meshes["sfera"], shaders["ShaderLab9"], modelMatrix, mapTextures["sky"]);
	}
	{ // sun
		num = 9;

		glm::mat4 modelMatrix = glm::mat4(1);;
		modelMatrix = glm::scale(modelMatrix, glm::vec3(5.0f));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 20, 0));
		RenderLightMesh(meshes["sfera"], shaders["ShaderLab9"], modelMatrix, mapTextures["sun"]);
	}

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(1, 1, -3));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(200));
		RenderSimpleMesh(meshes["sphere"], shaders["ShaderLab9"], modelMatrix, mapTextures["sky"]);
	}*/

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(17, 0, 30));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.4));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(170.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseB"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(270.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseR"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(50, 0, 5));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.3));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(0.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseW"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-10, 0, 50));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.3));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(270.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseK"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(40, 0, 25));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.3));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseG"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 20));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.3));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(270.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseQ"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 60));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.4));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(270.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseX"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 40));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.4));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(270.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseV"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-30, 0, 65));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.4));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(0.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["houseJ"]);
	}



	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-25, 0, 13));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(0.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(15, 0, 4));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 6));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(180.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 6));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(180.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-10, 0, 45));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(0.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, 57));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.03));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(0.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["banca"], shaders["ShaderLab9"], modelMatrix, mapTextures["banca"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-12, -1, -10));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		RenderSimpleMesh(meshes["Wood"], shaders["ShaderLab9"], modelMatrix, mapTextures["Wood"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-10, -1, -13));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["Wood"], shaders["ShaderLab9"], modelMatrix, mapTextures["Wood"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-6.5, -1, -10));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		RenderSimpleMesh(meshes["Wood"], shaders["ShaderLab9"], modelMatrix, mapTextures["Wood"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-9, 0.5, -10));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.17));
		RenderSimpleMesh(meshes["Fire"], shaders["ShaderLab9"], modelMatrix, mapTextures["Fire"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(69, 0.5, 15));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-64.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(22, 0.5, 15));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(64.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(53, 0.5, 25));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-23.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(32, 0.5, 40));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(64.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(65, 0.5, 42));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(125.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(78, 0.5, 19));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(105.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(55, 0.5, 55));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(99.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(70, 0.5, 24));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(167.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(86, 0.5, 20));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.0025));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(67.0f), glm::vec3(0, 0, 1));
		RenderSimpleMesh(meshes["Horse"], shaders["ShaderLab9"], modelMatrix, mapTextures["Horse"]);
	}

	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)

		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-55.f, 0, 8.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treebark"], shaders["ShaderLab9"], modelMatrix, mapTextures["treebark"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-55.f + i / 0.5f, 0, 8.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treeleaf"], shaders["ShaderLab9"], modelMatrix, mapTextures["treeleaf"]);
			}
		}
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)

		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(35.f, 0, 8.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treebark"], shaders["ShaderLab9"], modelMatrix, mapTextures["treebark"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(35.f + i / 0.5f, 0, 8.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treeleaf"], shaders["ShaderLab9"], modelMatrix, mapTextures["treeleaf"]);
			}
		}
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 3; j++)

		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-5.f + i / 0.5f, 0, 35.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treebark"], shaders["ShaderLab9"], modelMatrix, mapTextures["treebark"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-5.f + i / 0.5f, 0, 35.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treeleaf"], shaders["ShaderLab9"], modelMatrix, mapTextures["treeleaf"]);
			}
		}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-25, 0, 20));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["house2F"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(25, 0, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.5));
		//modelMatrix = glm::scale(modelMatrix, glm::vec3(0,0,0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(160.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["house2"], shaders["ShaderLab9"], modelMatrix, mapTextures["house2"]);
	}
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)

		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-25.f + i / 0.5f, 0, -30.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treebark"], shaders["ShaderLab9"], modelMatrix, mapTextures["treebark"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-25.f + i / 0.5f, 0, -30.f + j / 0.5f));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
				RenderSimpleMesh(meshes["treeleaf"], shaders["ShaderLab9"], modelMatrix, mapTextures["treeleaf"]);
			}
		}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(14, 0, 15));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
		RenderSimpleMesh(meshes["Lamp"], shaders["ShaderLab9"], modelMatrix, mapTextures["Lamp"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-35, 0, 15));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(-90.0f), glm::vec3(0, 1, 0));
		RenderSimpleMesh(meshes["Lamp"], shaders["ShaderLab9"], modelMatrix, mapTextures["Lamp"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-40, 0, -5));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.7));
		RenderSimpleMesh(meshes["Lamp"], shaders["ShaderLab9"], modelMatrix, mapTextures["Lamp"]);
	}

	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-60.f / 2.f, 0, 405.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(350));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}*/
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.f / 2.f, 0, -505.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(450));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-100.f / 2.f, 0, -505.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(450));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(80.f / 2.f, 0, -505.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(450));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}

	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f / 2.f, 0, -505.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(450));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-50, 0, -70));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(110));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	/*{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-90, 0, -70));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(110));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}*/
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(-90, 0, -70));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(120));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(0, 1, 0));

		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f / 2.f, 0, -405.f / 2.f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(350));
		RenderSimpleMesh(meshes["mountain"], shaders["ShaderLab9"], modelMatrix, mapTextures["mountain"]);
	}
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f, 0.5f, 0));
		modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

		modelMatrix = glm::scale(modelMatrix, glm::vec3(200));
		RenderSimpleMesh(meshes["square"], shaders["Shader2"], modelMatrix, mapTextures["grass"]);
	}
	//{ {
	//		glm::mat4 modelMatrix = glm::mat4(1);
	//		modelMatrix = glm::translate(modelMatrix, glm::vec3(60.f, 50.0f, 60));
	//		modelMatrix = glm::rotate(modelMatrix, RADIANS(20.0f), glm::vec3(1,0, 0));
	//		modelMatrix = RotateOY(unghi) * modelMatrix;
	//		//unghi += 0.03;
	//		modelMatrix = glm::scale(modelMatrix, glm::vec3(35));
	//		RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["clouds"]);
	//	}
	//{
	//	glm::mat4 modelMatrix = glm::mat4(1);
	//	modelMatrix = glm::translate(modelMatrix, glm::vec3(-60.f, 50.0f, 60));
	//	modelMatrix = glm::rotate(modelMatrix, RADIANS(20.0f), glm::vec3(1, 0, 0));
	//	modelMatrix = RotateOY(unghi) * modelMatrix;
	//	//unghi += 0.009;
	//	modelMatrix = glm::scale(modelMatrix, glm::vec3(35));
	//	RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["clouds"]);
	//}
	//{
	//	glm::mat4 modelMatrix = glm::mat4(1);
	//	modelMatrix = glm::translate(modelMatrix, glm::vec3(60.f, 50.0f, -60));
	//	modelMatrix = glm::rotate(modelMatrix, RADIANS(20.0f), glm::vec3(1, 0, 0));
	//	//modelMatrix = glm::rotate(modelMatrix, RADIANS(130.0f), glm::vec3(0, 1, 0));
	//	modelMatrix = RotateOY(unghi) * modelMatrix;
	//	unghi += 0.09;
	//	modelMatrix = glm::scale(modelMatrix, glm::vec3(35));
	//	RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["clouds"]);
	//}}
	//{ // Background
	//	glm::mat4 modelMatrix = glm::mat4(1);;
	//	modelMatrix = glm::scale(modelMatrix, glm::vec3(100.0f));
	//	modelMatrix = RotateOY(unghi) * modelMatrix;
	//	unghi += 0.09;
	//	RenderSimpleMesh(meshes["sfera"], shaders["ShaderLab9"], modelMatrix, mapTextures["clouds"]);
	//}
	for (int i = 0; i < 130; i++) {
		for (int j = 0; j < 3; j++)
		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-40.f + i / 2.f, 0.7f, 0 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-40.3f + i / 2.f, 0.7f, 0 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
		}
	}
	for (int i = 0; i < 90; i++) {
		for (int j = 0; j < 3; j++)
		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-10.f + i / 2.f, 0.6f, 17 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-10.3f + i / 2.f, 0.6f, 17 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
		}
	}
	for (int i = 0; i < 100; i++) {
		for (int j = 0; j < 3; j++)
		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(5.f + i / 2.f, 0.6f, 17 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(5.3f + i / 2.f, 0.6f, 17 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
		}
	}

	for (int i = 0; i < 80; i++) {
		for (int j = 0; j < 3; j++)
		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.f + i / 2.f, 0.6f, 20 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.3f + i / 2.f, 0.6f, 20 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
		}
	}

	for (int i = 0; i < 90; i++) {
		for (int j = 0; j < 3; j++)
		{
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.f + i / 2.f, 0.6f, 45 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
			{
				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, glm::vec3(-50.3f + i / 2.f, 0.6f, 45 + j / 2.f));
				modelMatrix = glm::rotate(modelMatrix, RADIANS(90.0f), glm::vec3(1, 0, 0));

				modelMatrix = glm::scale(modelMatrix, glm::vec3(3));
				RenderSimpleMesh(meshes["square"], shaders["ShaderLab9"], modelMatrix, mapTextures["stone"]);
			}
		}
	}
	//std::cout << "finish update";

}


void Laborator9::FrameEnd()
{
	DrawCoordinatSystem(camera->GetViewMatrix(), projectionMatrix);
	//std::cout << "FarmeEnd";

}

void Laborator9::RenderLightMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, Texture2D* texture1, Texture2D* texture2)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	// Bind view matrix
	glm::mat4 viewMatrix = camera->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	//glm::mat4 projectionMatrix = camera->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	//Trimit numarul obiectului la shader
	glUniform1i(glGetUniformLocation(shader->program, "num"), num);

	if (texture1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_1"), 0);
	}

	if (texture2)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, texture2->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_2"), 1);
	}

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}
void Laborator9::RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, Texture2D* texture1, Texture2D* texture2)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	// Bind view matrix
	glm::mat4 viewMatrix = camera->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	//glm::mat4 projectionMatrix = projectionMatrix;
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	glUniform1i(glGetUniformLocation(shader->program, "num"), num);
	glUniform1i(glGetUniformLocation(shader->program, "rotationFlag"), rotationFlag);
	glUniform1i(glGetUniformLocation(shader->program, "numLevels"), numLevels);
	glUniform1i(glGetUniformLocation(shader->program, "steppedLighting"), steppedLighting);
	glUniform1f(glGetUniformLocation(shader->program, "time"), Engine::GetElapsedTime());

	//Trimit pozitiile celor 6 lumini
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos1"), 1, glm::value_ptr(lightPosition[1]));
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos2"), 1, glm::value_ptr(lightPosition[2]));
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos3"), 1, glm::value_ptr(lightPosition[3]));
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos4"), 1, glm::value_ptr(lightPosition[4]));
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos5"), 1, glm::value_ptr(lightPosition[5]));
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos6"), 1, glm::value_ptr(lightPosition[6]));
	//Trimit pozitia soarelui
	glUniform3fv(glGetUniformLocation(shader->program, "light_pos7"), 1, glm::value_ptr(lightPosition[7]));

	//Trimit pozitia camerei
	glm::vec3 eyePosition = camera->position;
	int eye_position = glGetUniformLocation(shader->program, "eye_position");
	glUniform3f(eye_position, eyePosition.x, eyePosition.y, eyePosition.z);

	//Trimit shiness
	int material_shininess = glGetUniformLocation(shader->program, "material_shininess");
	glUniform1i(material_shininess, materialShininess);

	//Trimit kd
	int material_kd = glGetUniformLocation(shader->program, "material_kd");
	glUniform3f(material_kd, materialKd.x, materialKd.y, materialKd.z);

	//Trimit ks
	int material_ks = glGetUniformLocation(shader->program, "material_ks");
	glUniform3f(material_ks, materialKs.x, materialKs.y, materialKs.z);
	

	if (mesh == meshes["sfera"])
		glUniform1i(glGetUniformLocation(shader->program, "flag"), 1);
	else
		if (texture1 == mapTextures["random"])
			glUniform1i(glGetUniformLocation(shader->program, "flag"), 1);
		else
			glUniform1i(glGetUniformLocation(shader->program, "flag"), 0);
	if (mesh == meshes["sfera"] && (texture1 == mapTextures["sun"]))
		glUniform1f(glGetUniformLocation(shader->program, "time"), Engine::GetElapsedTime() / 30);
	if (mesh == meshes["sfera"] && (texture1 == mapTextures["sky"]))
		glUniform1f(glGetUniformLocation(shader->program, "time"), Engine::GetElapsedTime() / 500);
	else
		glUniform1f(glGetUniformLocation(shader->program, "time"), 0);
	if (mesh == meshes["square"] && (texture1 == mapTextures["clouds"]))
		glUniform1i(glGetUniformLocation(shader->program, "flag"), 2);
	if (mesh == meshes["Horse"] && (texture1 == mapTextures["Horse"]))
		glUniform1i(glGetUniformLocation(shader->program, "flag"), 4);
	if (mesh == meshes["square"] && (texture1 == mapTextures["stone"]))
		glUniform1i(glGetUniformLocation(shader->program, "flag"), 0);
	if (mesh == meshes["sfera"] && (texture1 == mapTextures["sun"]))
		glUniform1i(glGetUniformLocation(shader->program, "flag"), 5);

	if (texture1)
	{
		//TODO : activate texture location 0
		//TODO : Bind the texture1 ID
		//TODO : Send texture uniform value
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_1"), 0);
	}


	if (texture2)
	{
		//TODO : activate texture location 1
		//TODO : Bind the texture2 ID
		//TODO : Send texture uniform value
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, texture2->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_2"), 0);
	}

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

Texture2D* Laborator9::CreateRandomTexture(unsigned int width, unsigned int height)
{
	GLuint textureID = 0;
	unsigned int channels = 3;
	unsigned int size = width * height * channels;
	unsigned char* data = new unsigned char[size];

	// TODO: generate random texture data
	for (int i = 0; i < size; ++i)
		data[i] = rand() % 128;

	// Generate and bind the new texture ID
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	// TODO: Set the texture parameters (MIN_FILTER, MAG_FILTER and WRAPPING MODE) using glTexParameteri
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);

	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	CheckOpenGLError();

	// TODO: Use glTextImage2D to set the texture data
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	// TODO: Generate texture mip-maps
	glGenerateMipmap(GL_TEXTURE_2D);
	CheckOpenGLError();

	// Save the texture into a wrapper Texture2D class for using easier later during rendering phase
	Texture2D* texture = new Texture2D();
	texture->Init(textureID, width, height, channels);

	SAFE_FREE_ARRAY(data);
	return texture;
}

// Documentation for the input functions can be found in: "/Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator9::OnInputUpdate(float deltaTime, int mods)
{
	// move the camera only if MOUSE_RIGHT button is pressed
	//if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float cameraSpeed = 2.0f;
		deltaTime *= 2;

		if (window->KeyHold(GLFW_KEY_W)) {
			// TODO : translate the camera forward
			if (camera->getPosition().y > 2)
			camera->TranslateForward(cameraSpeed * deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_A)) {
			// TODO : translate the camera to the left
			camera->TranslateRight(-cameraSpeed * deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_S)) {
			// TODO : translate the camera backwards
			if (camera->getPosition().y > 2)
			camera->TranslateForward(-cameraSpeed * deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_D)) {
			// TODO : translate the camera to the right
			camera->TranslateRight(cameraSpeed * deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_Q)) {
			// TODO : translate the camera down
			if(camera->getPosition().y>2)
			camera->TranslateUpword(-cameraSpeed * deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_E)) {
			// TODO : translate the camera up
			camera->TranslateUpword(cameraSpeed * deltaTime);
		}
	}

	if (window->KeyHold(GLFW_KEY_U)) {
		fov += deltaTime;
		projectionMatrix = glm::perspective(RADIANS(60), fov, 0.01f, 200.0f);
	}
	//if (window->KeyHold(GLFW_KEY_I)) {
	//	if (fov > 2 * deltaTime)
	//		fov -= deltaTime;
	//	projectionMatrix = glm::perspective(RADIANS(60), fov, 0.01f, 200.0f);
	//}

	if (window->KeyHold(GLFW_KEY_K)) {
		if (ok) {
			dx1 -= deltaTime;
			dx2 += deltaTime;
			projectionMatrix = glm::ortho(dx1, dx2, dy1, dy2, 0.1f, 200.0f);
		}
	}

	if (window->KeyHold(GLFW_KEY_L)) {
		if (ok) {
			dx1 += deltaTime;
			dx2 -= deltaTime;
			projectionMatrix = glm::ortho(dx1, dx2, dy1, dy2, 0.1f, 200.0f);
		}
	}

	if (window->KeyHold(GLFW_KEY_N)) {
		if (ok) {
			dy1 -= deltaTime;
			dy2 += deltaTime;
			projectionMatrix = glm::ortho(dx1, dx2, dy1, dy2, 0.1f, 200.0f);
		}
	}

	if (window->KeyHold(GLFW_KEY_M)) {
		if (ok) {
			dy1 += deltaTime;
			dy2 -= deltaTime;
			projectionMatrix = glm::ortho(dx1, dx2, dy1, dy2, 0.1f, 200.0f);
		}
	}
}

void Laborator9::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_I) {
		steppedLighting = 1 - steppedLighting;
	}
	if (key == GLFW_KEY_T)
	{
		renderCameraTarget = !renderCameraTarget;
	}

	if (key == GLFW_KEY_O) {
		ok = 1;
		projectionMatrix = glm::ortho(dx1, dx2, dy1, dy2, 0.1f, 200.0f);
	}

	if (key == GLFW_KEY_P) {
		projectionMatrix = glm::perspective(RADIANS(45), window->props.aspectRatio, 0.01f, 200.0f);
	}
}

void Laborator9::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator9::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event

	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float sensivityOX = 0.001f;
		float sensivityOY = 0.001f;

		if (window->GetSpecialKeyState() == 0) {
			renderCameraTarget = false;
			// TODO : rotate the camera in First-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->RotateFirstPerson_OX(sensivityOY * deltaY);
			camera->RotateFirstPerson_OY(sensivityOX * deltaX);
		}

		if (window->GetSpecialKeyState() && GLFW_MOD_CONTROL) {
			renderCameraTarget = true;
			// TODO : rotate the camera in Third-person mode around OX and OY using deltaX and deltaY
			// use the sensitivity variables for setting up the rotation speed
			camera->RotateThirdPerson_OX(sensivityOY * deltaY);
			camera->RotateThirdPerson_OY(sensivityOX * deltaX);
		}

	}
}



void Laborator9::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator9::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator9::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator9::OnWindowResize(int width, int height)
{
}
